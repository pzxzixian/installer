Format: 3.0 (native)
Source: linuxcnc
Binary: linuxcnc-uspace-dev, linuxcnc-uspace, linuxcnc-doc-en, linuxcnc-doc-fr, linuxcnc-doc-es
Architecture: any all
Version: 1:2.8.0~pre1
Maintainer: Sebastian Kuzminsky <seb@highlab.com>
Standards-Version: 3.9.2
Vcs-Browser: https://github.com/LinuxCNC/linuxcnc
Vcs-Git: git://github.com/linuxcnc/linuxcnc.git
Build-Depends: debhelper (>= 6), dh-python, libudev-dev, iptables, kmod, python-yapps, tcl8.6-dev, tk8.6-dev, libreadline-gplv2-dev, asciidoc (>= 8.5), dblatex (>= 0.2.12), docbook-xsl, dvipng, ghostscript, graphviz, groff, imagemagick, inkscape, python-lxml, source-highlight, texlive-extra-utils, texlive-font-utils, texlive-fonts-recommended, texlive-lang-cyrillic, texlive-lang-french, texlive-lang-german, texlive-lang-polish, texlive-lang-spanish, texlive-latex-recommended, w3c-linkchecker, xsltproc, asciidoc-dblatex, python, python-dev, python-tk, libxmu-dev, libglu1-mesa-dev, libgl1-mesa-dev | libgl1-mesa-swx11-dev, libgtk2.0-dev, gettext, intltool, autoconf, libboost-python-dev, netcat, libmodbus-dev (>= 3.0), libusb-1.0-0-dev, procps, psmisc, desktop-file-utils, yapps2
Package-List:
 linuxcnc-doc-en deb misc extra arch=all
 linuxcnc-doc-es deb misc extra arch=all
 linuxcnc-doc-fr deb misc extra arch=all
 linuxcnc-uspace deb misc extra arch=any
 linuxcnc-uspace-dev deb libs extra arch=any
Checksums-Sha1:
 20945c85083b2b4d5ed46beaaec45d201e93b491 32956204 linuxcnc_2.8.0~pre1.tar.xz
Checksums-Sha256:
 7f46f6112d91ebfd4e5bf9cbdecb8c952d442a89875a8c5769b7f1a494291822 32956204 linuxcnc_2.8.0~pre1.tar.xz
Files:
 69a70bcd6879fc1b8919bf123d7a73a9 32956204 linuxcnc_2.8.0~pre1.tar.xz
