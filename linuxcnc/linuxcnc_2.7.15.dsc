Format: 3.0 (native)
Source: linuxcnc
Binary: linuxcnc-uspace-dev, linuxcnc-uspace, linuxcnc-doc-en, linuxcnc-doc-fr, linuxcnc-doc-es
Architecture: any all
Version: 1:2.7.15
Maintainer: Sebastian Kuzminsky <seb@highlab.com>
Standards-Version: 2.1.0
Build-Depends: debhelper (>= 6), libudev-dev, iptables, dvipng, texlive-extra-utils, texlive-latex-recommended, texlive-fonts-recommended, ghostscript, imagemagick, texlive-lang-polish, texlive-font-utils, kmod, dh-python, asciidoc-dblatex, tcl8.6-dev, tk8.6-dev, libxaw7-dev, libncurses-dev, libreadline-gplv2-dev, asciidoc (>= 8.5), source-highlight, dblatex (>= 0.2.12), xsltproc, groff, docbook-xsl, python, python-dev, python-tk, python-lxml, libglu1-mesa-dev, libgl1-mesa-dev | libgl1-mesa-swx11-dev, libgtk2.0-dev, gettext, autoconf, libboost-python-dev, texlive-lang-cyrillic, texlive-lang-french, texlive-lang-spanish, texlive-lang-german, netcat, libmodbus-dev (>= 3.0), libusb-1.0-0-dev, procps, psmisc, graphviz, inkscape, intltool, desktop-file-utils, yapps2, python-yapps
Package-List:
 linuxcnc-doc-en deb misc extra arch=all
 linuxcnc-doc-es deb misc extra arch=all
 linuxcnc-doc-fr deb misc extra arch=all
 linuxcnc-uspace deb misc extra arch=any
 linuxcnc-uspace-dev deb libs extra arch=any
Checksums-Sha1:
 5699230c6f0b527d2a5fa81514c66d61031f410e 20868760 linuxcnc_2.7.15.tar.xz
Checksums-Sha256:
 5cb9958b1bac647c0643f63817fe8853b9104a5d8a007bee053ead6cd02eee89 20868760 linuxcnc_2.7.15.tar.xz
Files:
 3df6a30c16d22f921c473fc067ad2cf9 20868760 linuxcnc_2.7.15.tar.xz
